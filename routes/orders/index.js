const router = require('express').Router()
const validate = require("./validator");
const controllers = require('./controller')

router.get('/get_stock', validate.getStock,controllers.getStock)
router.post('/sell',  controllers.sellStock)
router.post('/getorder',  controllers.getOrder)


module.exports = router