const order = require("./order.json");
const stock = require("./stock.json");
const messages = require('../../messages')
const user = require("../users/users.json");


const controllers = {};
const { writeFile, writeFileSync } = require("fs");

controllers.getStock = (req, res) => {
  return res.json({ data: order });
};
controllers.sellStock = (req, res) => {
  const { email, item } = req.body;
  if (stock.some((ele) => req.body.item == ele.item && ele.quantity > 0)) {
    const st = { item, email, id: Date.now() };
    order.push(st);
    writeFile(require("path").join(__dirname, "order.json"),
      JSON.stringify(order),
      (err) => {
        if (err) return res.status(messages.status.badRequest).res.json(messages.messages.Error);
      }
    );
    const upStock = stock.map((ele) =>
      ele.item == item ? { ...ele, quantity: parseInt(ele.quantity - 1) } : ele
    );
    writeFileSync(
      require("path").join(__dirname, "stock.json"),JSON.stringify(upStock));
    user[user.findIndex((ele) => ele.email === email)].orderArray = [st];
    writeFileSync(
      require("path").join(__dirname, "../users/users.json"),
      JSON.stringify(user)
    );
    return res.json(messages.messages.order);
  } else {
    return res.json(messages.messages.outOfStock);
  }
};
controllers.getOrder = (req, res) => {
  const { email } = req.body;
  const userorder = order.filter((ele) => ele.email == email);
  res.json(userorder);
};
module.exports = controllers;
