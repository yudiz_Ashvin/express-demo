const validators = {}
const user = require('./users.json')

const messages = require('../../messages')

validators.getUsers = (req,res,next) => {
  const { sEmail } = req.body;
  if(user.some((ele) => ele.email === sEmail && ele.type !== 'Admin')){
return    res
      .status(messages.status.badRequest)
      .json(messages.messages.unAuthorized);
  } else {
    next();
  }
}
validators.signUp = (req, res, next) => {
  const { fName, lName, email, password } = req.body
  if (!email) return res.status(403).json({ message: 'Email is requierd' })
  if (!password) return res.status(403).json({ message: 'Password is requierd' })
  if (!fName) return res.status(403).json({ message: 'Firstname is requierd' })
  if (!lName) return res.status(403).json({ message: 'Lastname is requierd' })
  next()
}

validators.login = (req, res, next) => {
  const { sEmail, sPassword } = req.body
  if (!sEmail) return res.status(messages.status.statusNotFound).json(messages.messages.emailReq)
  if (!sPassword) return res.status(messages.status.statusNotFound).json(messages.messages.passReq)
  next()
}

validators.editProfile = (req, res, next) => {
  const { sEmail, sPassword } = req.body;
  if (
    user.findIndex((ele) => ele.email === sEmail) > -1 &&
    user[user.findIndex((ele) => ele.email === sEmail)].password ===
      sPassword
  )
    return res
      .status(messages.status.statusSuccess)
      .json(messages.messages.process);

  if (user.findIndex((ele) => ele.email !== sEmail) > -1 && user[user.findIndex((ele) => ele.email !== sEmail)].password !==sPassword
  )
    return res
      .status(messages.status.statusNotFound)
      .json(messages.messages.invalidCredentials);
  next();
};


module.exports = validators
