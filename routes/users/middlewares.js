const user = require('./users.json')
const messages = require('../../messages')


const middleware = {}


middleware.changePass = (req, res, next) => {
  const { eEmail } = req.body
  if(user.some((ele) => ele.email === eEmail && ele.isLoggedIn !== true)){
    return res.status(messages.status.badRequest).json(messages.messages.logIn);
  }else{
    next()
  }
}
middleware.editProfile = (req, res, next) => {
  const { eEmail } = req.body
  if(user.some((ele) => ele.email === eEmail && ele.isLoggedIn !== true)){
    return res.status(messages.status.badRequest).json(messages.messages.logIn);
  }else{
    next()
  }
}
middleware.delete = (req, res, next) => {
  const { eEmail } = req.body

  if(user.some((ele) => ele.email === eEmail && ele.isLoggedIn !== true)){
    return res.status(messages.status.badRequest).json(messages.messages.logIn);
  }else{
    next()
  }
}

middleware.getUsers = (req,res,next) => {
  const { sEmail } = req.body;
  if(user.some((ele) => ele.email === sEmail && ele.type !== 'Admin')){
return    res
      .status(messages.status.badRequest)
      .json(messages.messages.unAuthorized);
  } else {
    next();
  }
}

module.exports = middleware