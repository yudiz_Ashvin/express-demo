const user = require('./users.json')
const crypto = require('crypto-js')
require('dotenv').config()
const jwt = require('jsonwebtoken');
const controllers = {}
const messages = require('../../messages')
const { writeFileSync } = require('fs');


controllers.getUsers = (req, res) => {
  const users = user
  return res.json({ message: 'User fetch success', data: users })
}
//signup
controllers.signUp = (req, res) => {
  const { fName, lName, email, password, type } = req.body

  if (user.findIndex((ele) => ele.email === email) > -1) 
    return res.status(406).json({ message: 'emailId already exists' })
    const ciphertext = crypto.AES.encrypt(password, process.env.KEY).toString();
    user.push({
      fName: req.body.fName,
      lName: req.body.lName,
      email: req.body.email,
      password: ciphertext,
      type: req.body.type,
    });
    // user.push(req.body)
    writeFileSync(require('path').join(__dirname, 'users.json'), JSON.stringify(user))
    return res.status(messages.status.statusSuccess).json(messages.messages.registeredSuccess)
}

//signin
controllers.login = (req, res) => {
  let { sEmail, sPassword } = req.body;
  const us = user.findIndex((ele) => ele.email === sEmail)
  var token = jwt.sign({ sEmail }, process.env.secret, { expiresIn: "1s" });
  let bytes = crypto.AES.decrypt(
    user[user.findIndex((ele) => ele.email === sEmail)].password,process.env.KEY);
  let originalText = bytes.toString(crypto.enc.Utf8);
  // if (
  //   user[user.findIndex((ele) => ele.email === sEmail)].isLoggedIn === true
  // ) {
  //   return res
  //     .status(messages.status.badRequest)
  //     .json(messages.messages.alreadyLoggedin);
  // }

  if (us > -1 && originalText === sPassword) {
    user[us].isLoggedIn = true; 
    user[us].orderArray = [];
    user[us].token = [];
// for(let i=0; i<5;i++){

// if(user[us].token[0] === user[us].token[1]){
//   user[us].token = token
// }else{
//   return res.status(messages.status.statusSuccess).json({messages:"hoo"});}
// }
  // user[us].token.push(token)
    writeFileSync(require("path").join(__dirname, "users.json"),JSON.stringify(user));

    return res.status(messages.status.statusSuccess).json(messages.messages.loginSuccess);
  }
  if (us > -1 && originalText !=sPassword) {
    return res.status(messages.status.badRequest).json(messages.messages.wrongpass);
  } else {
    return res.status(messages.status.statusNotFound).json(messages.messages.EmailNotfound);
  }
}
//delete
controllers.delete = (req,res)=>{
  const {eEmail, sPassword } = req.body;
  let del = user.findIndex((ele) => ele.email === eEmail)
  if(del > -1 && user[del].password === sPassword ){
    if(user[del].isLoggedIn === true ){
    user.splice(del,1);
    writeFileSync(require("path").join(__dirname, "users.json"),JSON.stringify(user));
    return res.json({ message: "User Deleted" });
  }else {
    res.json({ message: "login first" });
  }
}else{
  res.json({ message: "user not found" });
}
}

//change pass
controllers.changePass = (req, res) => {
  const { eEmail, oPassword, nPassword } = req.body;
  const passM = user.some((ele) => ele.email === eEmail && ele.password === oPassword);
  const passNotM = user.some((ele) => ele.email === eEmail && ele.password !== oPassword);
  if (passM) { 
    user.forEach((ele) => { if (ele.password === oPassword) ele.password = nPassword ? nPassword : ele.password});
    writeFileSync(require("path").join(__dirname, "users.json"),JSON.stringify(user));
    return res.status(messages.status.statusSuccess).json({message: "changed Password successfully"});
  }
  if(passNotM){
    return res.status(messages.status.statusNotFound).json({message: "Password doesnt match"});
  } else {
    return res.status(messages.status.statusNotFound).json({message: "Email is require"});
  }
}
//edit profile
controllers.editProfile = (req, res) => {
  const { fName, lName, email } = req.body;
  const find = user.some((ele) => ele.email === email);
  if (find) {
    user.forEach((ele) => {
      if (ele.email === email) {
        ele.fName = fName ? fName : ele.fName;
        ele.lName = lName ? lName : ele.lName;
      }});
    writeFileSync(require("path").join(__dirname, "users.json"),JSON.stringify(user));
    return res.status(messages.status.statusSuccess).json(messages.messages.updatedProfile);
  } else {
    return res.status(messages.status.statusNotFound).json(messages.messages.loginEmailNotRegistered);
  }
}

module.exports = controllers