const router = require('express').Router()
const middlewareWrapper = require('cors')
const controllers = require('./controllers')
const validators = require('./validators')
const middleware = require('./middlewares')

router.get('/get-users', middleware.getUsers,validators.getUsers,controllers.getUsers)
router.post('/login', validators.login, controllers.login)
router.post('/signup', validators.signUp, controllers.signUp)
router.delete('/delete',middleware.delete,controllers.delete)
router.post('/changepass',middleware.changePass,controllers.changePass)
router.post('/editprofile',middleware.editProfile,controllers.editProfile)

module.exports = router
